'use strict'

/**
 * 簡易雪花算法
 * 每次呼叫傳回一個不同 32bit 整數
 * 因為長度不夠很快會出現重複值, 不能作為鍵值或唯一值
 */
const frost = (() => {
  const KICKOFF = 1646210542595 // 2022-03-02T08:42:22.595Z
  const KEEP = 8
  const MAX = Math.pow(2, KEEP) // eq.256 = 0b11111111+1
  const MASK = MAX - 1 // 255
  const getRandom = () => (Math.random() * MAX) | 0
  const nextTimestamp = (last) => {
    let stamp = Date.now()
    while (last >= stamp) { // 保證與上一次不同
      stamp = Date.now()
    }
    return stamp
  }

  let lastTimestamp = -1
  let seq = 0
  return () => {
    let timestamp = Date.now()
    if (lastTimestamp === timestamp) {
      seq = (seq + 1) & MASK // 間隔短時間內直接遞增
      if (!seq) { // 歸零後重新取初值
        seq = getRandom()
        timestamp = nextTimestamp(lastTimestamp)
      }
    } else { // 間隔長時間則重新取初值
      seq = getRandom()
    }
    lastTimestamp = timestamp
    // 前置時間差, 後置隨機序號, 最後取正整
    return ((timestamp - KICKOFF) << KEEP | seq) >>> 0
  }
})()

export {
  frost
}
